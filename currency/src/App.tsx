import React, {useState} from 'react'
import './App.css';
import {useRoutes} from "hookrouter";
import NotFound from "./components/NotFound/NotFound";
import Home from "./components/Home/Home";
import BaseCtx from "./contexts/BaseCtx";
import UserFriendlyMessage from "./components/UserFirendlyMessage/UserFirendlyMessage";

function App() {

    const [base, setBase] = useState<string>('EUR')

    const routes = {
        "/" : () => <Home/>,
    }

    function updateBase(name:string){
        setBase(name)
    }

    function showMessage() {
        if(!localStorage.getItem('message')){
            return true;
        }
        return false;
    }

    const routeResult = useRoutes(routes)

    return (
        <div className="App">
            { showMessage() && <UserFriendlyMessage/> }
            <BaseCtx.Provider value={{base:base, updateBase:updateBase}}>
            { routeResult || <NotFound/> }
            </BaseCtx.Provider>
        </div>
    );
}

export default App;
