import React, {useState} from 'react'
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

const UserFriendlyMessage:React.FC = () => {

    const [modal, setModal] = useState(true)

    function toggle(){
        setModal(!modal)
    }

    localStorage.setItem('message', 'true')

    return(
        <Modal isOpen={modal} toggle={toggle}>
            <ModalHeader toggle={toggle}>WELCOME!</ModalHeader>
            <ModalBody>
                This is a user friendly message: HI! :)
            </ModalBody>
            <ModalFooter>
                <Button color="success" onClick={toggle}>Ok</Button>
            </ModalFooter>
        </Modal>
    );
}

export default UserFriendlyMessage