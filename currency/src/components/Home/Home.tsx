import React, {useContext, useEffect, useState} from "react";
import BaseCtx from "../../contexts/BaseCtx";
import axios from "axios";
import {latestDto} from "../../types/latestDto";
import {ButtonDropdown, DropdownItem, DropdownMenu, DropdownToggle, Input, Jumbotron, Spinner, Table} from "reactstrap";
import Converter from "./Converter/Converter";

//todo: switch convert in converter, user message on first visit, switch base curr

const Home:React.FC = () => {

    const [target, setTarget] = useState<{name: string, data: number} | null>(null)
    const [search, setSearch] = useState('')
    const [formattedRates, setFormattedRates] = useState<any[]>([])
    const [filteredRates, setFilteredRates] = useState<any[]>([])
    const [isOpen, setIsOpen] = useState(false)

    const base = useContext(BaseCtx)

    function formatRates(obj: latestDto){
        const arr:{name:string, data:number}[] = []
        const keys = Object.keys(obj.rates);
        let i = 0;
        Object.values(obj.rates).forEach(rate =>
            {
                arr.push({name: keys[i], data: rate});
                i++
            }
        );
        console.log('size of latest:' + arr.length)
        setFormattedRates(arr)
        setFilteredRates(arr)
    }

    function handleTargetSelection(name:string, data:number){
        setTarget({name:name, data:data})
    }

    function handleSearchChange(e:any){
        setSearch(e.target.value)
        let currentList = [];
        let newList = [];

        if (e.target.value !== "") {
            currentList = formattedRates;
            newList = currentList.filter(rate => {
                const lc = rate.name.toLowerCase();
                const filter = e.target.value.toLowerCase();
                return lc.includes(filter);
            });
        } else {
            newList = formattedRates;
        }
        setFilteredRates(newList)
    }

    function toggle(){
        setIsOpen(!isOpen)
    }

    function handleBaseSelect(name:string){
        base.updateBase(name);
    }

    useEffect(() => {
        let isSubscribed = true;
        axios.get("https://api.ratesapi.io/api/latest?base=" + base.base)
            .then((response) =>
                isSubscribed? (
                    console.log('latest'),
                    formatRates(response.data),
                    setSearch(''),
                    setTarget(null)
                ) :
                    null
                )
            .catch((err) => isSubscribed? (console.log('latest error' + err)) : null)
        return () => {isSubscribed = false}
    },[base.base])

    return(
        <div className="home">
            <div className="homeOne">
                <Jumbotron>
                <h1 className="display-4">Your base currency is
                    <ButtonDropdown isOpen={isOpen} toggle={toggle}>
                        <DropdownToggle caret color="secondary">
                            {base.base}
                        </DropdownToggle>
                        <DropdownMenu style={{height:"100px", overflowY:"scroll"}}>
                            {filteredRates.map(rate =>
                                <DropdownItem onClick={() => handleBaseSelect(rate.name)}>{rate.name}</DropdownItem>
                            )}
                        </DropdownMenu>
                    </ButtonDropdown>
                </h1>
                {
                    target && <Converter name={target.name} data={target.data}/>
                ||
                    !target && <p>Select a currency from the list to convert</p>
                }
                </Jumbotron>
            </div>
            <div className="homeTwo">
                {
                    formattedRates? (
                        <>
                            <div className="currencyTable">
                                <Input value={search} type="text" placeholder="Search" onChange={handleSearchChange}/>
                                <Table borderless>
                                    <tbody>
                                    {filteredRates.map(rate =>
                                        <tr onClick={() => handleTargetSelection(rate.name, rate.data)}>
                                            <td>{rate.name}</td>
                                            <td>{rate.data}</td>
                                        </tr>
                                    )}
                                    </tbody>
                                </Table>
                            </div>
                        </>
                    ) : (
                        <>
                            <p>Loading..</p>
                            <Spinner color="secondary" />
                        </>
                    )
                }
            </div>
        </div>
    )
}

export default Home