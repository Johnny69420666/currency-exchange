import React from "react";

const NotFound:React.FC = () => {
    return(
        <>
            <h1>404.</h1>
            <p>We could'nt find what you were looking for.</p>
        </>
    )
}

export default NotFound